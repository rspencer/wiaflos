# Year end procedures
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::YearEnd;

use strict;
use warnings;

use wiaflos::server::api::auth;

use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::YearEnd;


# Plugin info
our $pluginInfo = {
	Name => "YearEnd",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/YearEnd','performYearEnd','YearEnd/Perform');
}




## @fn performYearEnd
# Function to perform a yearend on the set of books
#
# @param data Parameter hash ref
# @li StartDate - Optional yearend start date
# @li EndDate - Optional yearend end date
# @li REAccountNumber - Retained earnings account number
# @li Type - GL entry types to run the year-end for
sub performYearEnd {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'StartDate'}) || $data->{'StartDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'StartDate' invalid");
	}

	if (!defined($data->{'EndDate'}) || $data->{'EndDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'EndDate' invalid");
	}

	if (!defined($data->{'REAccountNumber'}) || $data->{'REAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'REAccountNumber' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'REAccountNumber'} = $data->{'REAccountNumber'};
	$detail->{'Type'} = $data->{'Type'};
	my $res = wiaflos::server::core::YearEnd::performYearEnd($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::YearEnd::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
