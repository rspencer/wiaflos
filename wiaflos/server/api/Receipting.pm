# SOAP interface to Receipting module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Receipting;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Receipting;


# Plugin info
our $pluginInfo = {
	Name => "Receipting",
	Init => \&init,
};



# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','createReceipt','Receipting/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','getReceipts','Receipting/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','getReceipt','Receipting/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','postReceipt','Receipting/Post');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','sendReceipt','Receipting/Send');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','createReceiptAllocation','Receipting/Allocation/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','getReceiptAllocations','Receipting/Allocation/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Receipting','postReceiptAllocation','Receipting/Allocation/Post');
}





# Sanitize receipt
sub sanitizeReceipt
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'ClientID'} = $param->{'ClientID'};
	$tmpItem->{'ClientCode'} = $param->{'ClientCode'};

	$tmpItem->{'GLAccountID'} = $param->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $param->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TransactionDate'} = $param->{'TransactionDate'};
	$tmpItem->{'Reference'} = $param->{'Reference'};
	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	$tmpItem->{'Closed'} = $param->{'Closed'};

	return $tmpItem;
}


# Create receipt
# Parameters:
#		ClientCode	- Client code
#		GLAccountNumber	- GL account where money was paid from
#		Number	- Receipt number
#		Date		- Date of receipt
#		Reference			- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createReceipt {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ClientCode'}) || $data->{'ClientCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ClientCode' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ClientCode'} = $data->{'ClientCode'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Date'} = $data->{'Date'};
	$detail->{'Reference'} = $data->{'Reference'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::Receipting::createReceipt($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Receipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of receipts
sub getReceipts {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::Receipting::getReceipts($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Receipting::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeReceipt($item));
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return a receipt
sub getReceipt {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'ID'} = $data->{'ID'};
	my $rawData = wiaflos::server::core::Receipting::getReceipt($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Receipting::Error());
	}

	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};

	$tmpItem->{'ClientID'} = $rawData->{'ClientID'};
	$tmpItem->{'ClientCode'} = $rawData->{'ClientCode'};

	$tmpItem->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $rawData->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $rawData->{'Number'};

	$tmpItem->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$tmpItem->{'Reference'} = $rawData->{'Reference'};
	$tmpItem->{'Amount'} = $rawData->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $rawData->{'Posted'};

	$tmpItem->{'Closed'} = $rawData->{'Closed'};


	return SOAPResponse(RES_OK,$tmpItem);
}


# Post receipt
# Parameters:
#		Number	- Receipt number
sub postReceipt
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::Receipting::postReceipt($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Receipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Post allocation
# Parameters:
#		ID	- Allocation ID to post
sub postReceiptAllocation
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $res = wiaflos::server::core::Receipting::postReceiptAllocation($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Receipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# @fn createReceiptAllocation($data)
# Create a receipt allocation
#
# @param data Hash ref with the below elements
# @li ReceiptNumber - Receipt number to create allocation for
# @li InvoiceNumber - Invoice number to allocate to
# @li TransactionNumber - Transaction number to allocate to
# @li Amount - Amount to allocate
#
# @return Receipt allocation ID on success < 1 on error
sub createReceiptAllocation {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ReceiptNumber'}) || $data->{'ReceiptNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ReceiptNumber' invalid");
	}

	if (defined($data->{'InvoiceNumber'})) {
		if ($data->{'InvoiceNumber'} eq "") {
			return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'InvoiceNumber' invalid");
		}
	} elsif (defined($data->{'TransactionNumber'})) {
		if ($data->{'TransactionNumber'} eq "") {
			return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TransactionNumber' invalid");
		}
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ReceiptNumber'} = $data->{'ReceiptNumber'};
	$detail->{'InvoiceNumber'} = $data->{'InvoiceNumber'};
	$detail->{'TransactionNumber'} = $data->{'TransactionNumber'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::Receipting::createReceiptAllocation($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Receipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of receipt allocations
sub getReceiptAllocations {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ReceiptNumber'}) || $data->{'ReceiptNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ReceiptNumber' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'ReceiptNumber'} = $data->{'ReceiptNumber'};
	my $rawData = wiaflos::server::core::Receipting::getReceiptAllocations($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Receipting::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'ReceiptNumber'} = $item->{'ReceiptNumber'};

		$tmpItem->{'InvoiceID'} = $item->{'InvoiceID'};
		$tmpItem->{'InvoiceNumber'} = $item->{'InvoiceNumber'};

		$tmpItem->{'Amount'} = $item->{'Amount'};

		$tmpItem->{'GLTransactionID'} = $item->{'GLTransactionID'};
		$tmpItem->{'Posted'} = $item->{'Posted'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}



# Build client receipt
# Parameters:
#		Number	- Receipt number
#		SendTo		- Send receipt here
sub sendReceipt {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'SendTo'}) || $data->{'SendTo'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SendTo' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'SendTo'} = $data->{'SendTo'};
	my $res = wiaflos::server::core::Receipting::sendReceipt($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Receipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}




1;
# vim: ts=4
