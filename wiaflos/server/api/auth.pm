# SOAP authentication functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


# Predefined groups:
#	everybody
#	admins



# Authentication package
package wiaflos::server::api::auth;

use strict;
use warnings;

use wiaflos::version;


require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
);



# Authentication data
my $authData;

# Access control list
my @acl;



# Initialize ACLs, this sets our $server variable
my $server;
sub init {
	my $tmp = shift;

	$server = $tmp;
}


# Add ACL item
sub aclAdd {
	my ($package,$function,$capability) = @_;

	my $itemInfo;

	# Setup item info
	$itemInfo->{'package'} = $package;
	$itemInfo->{'function'} = $function;
	$itemInfo->{'capability'} = $capability;

#	printf(STDERR 'Adding %s::%s to capability "%s"'."\n",$package,$function,$capability);
	push(@acl,$itemInfo);

	# Map function into dispatch
	$server->map_dispatch($package,$function);
}


# Set session data
sub sessionSetData {
	my %data = @_;

	# Set and reset data
	$authData = \%data;
}


# Unset session data
sub sessionUnsetData {
	my $data = shift;

	# Set and reset data
	$authData = undef;
}


# Return session data
sub sessionGetData {
	return $authData;
}


# Return 0 if we authorized, -1 if not
sub checkAccess {
	my ($uri,$package,$function) = @_;


	# Check we have session data
	if (!defined($authData)) {
		die "Access denied. No active session";
	}

	# Loop with access controls
	foreach my $i (@acl) {
		# Next if we don't match
		next if ($i->{'package'} ne $package || $i->{'function'} ne $function);

		# Loop with uses capabilities
		foreach my $capability (@{$authData->{'Capabilities'}}) {
				# Check if our capability matches
				if (lc($i->{'capability'}) eq lc($capability)) {
					$authData->{'_server'}->log(4,"[AUTH] Function ${package}::${function} accessed by '".$authData->{'Username'}."' [".$authData->{'Peer'}."]");
					return;
				}
		}
	}

	# If we got here, we didn't return, so we not authorized
	$authData->{'_server'}->log(1,"[AUTH] Access denied to ${package}::${function} from '".$authData->{'Username'}."' [".$authData->{'Peer'}."]");
	die "Access denied to ${package}::${function}";
}


1;
# vim: ts=4
