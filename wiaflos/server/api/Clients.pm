# SOAP interface to Clients module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Clients;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Clients;


# Plugin info
our $pluginInfo = {
	Name => "Clients",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClients','Clients/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClient','Clients/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClientAddresses','Clients/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClientPhoneNumbers','Clients/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClientEmailAddresses','Clients/Show');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','createClient','Clients/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','linkClientAddress','Clients/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','linkClientEmailAddress','Clients/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','linkClientPhoneNumber','Clients/Add');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','removeClient','Clients/Remove');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','createAccountTransaction','Clients/GL/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getAccountTransactions','Clients/GL/List');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getAccountTransaction','Clients/GL/Get');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','postAccountTransaction','Clients/GL/Post');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getAccountTransactionAllocations','Clients/GL/Get');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Clients','getClientGLAccountEntries','Clients/GL/Show');
}



# Return a single client
# Optional:
#		ID	 - Client ID
#		Code   - Client code
sub getClient {
	my (undef,$data) = @_;
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Check params
	if (!defined($data->{'ID'}) && !defined($data->{'Code'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' or 'Code' must be provided");
	}

	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	$detail->{'Code'} = $data->{'Code'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getClient($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};
	$tmpItem->{'Code'} = $rawData->{'Code'};
	$tmpItem->{'Name'} = $rawData->{'Name'};
	$tmpItem->{'RegNumber'} = $rawData->{'RegNumber'};
	$tmpItem->{'TaxReference'} = $rawData->{'TaxReference'};
	$tmpItem->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $rawData->{'GLAccountNumber'};
	$tmpItem->{'ContactPerson'} = $rawData->{'ContactPerson'};


	return SOAPResponse(RES_OK,$tmpItem);
}


# Return list of clients
sub getClients {
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getClients();
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Code'} = $item->{'Code'};
		$tmpItem->{'Name'} = $item->{'Name'};
		$tmpItem->{'RegNumber'} = $item->{'RegNumber'};
		$tmpItem->{'TaxReference'} = $item->{'TaxReference'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};
		$tmpItem->{'ContactPerson'} = $item->{'ContactPerson'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Create client
# Parameters:
#		Code		- Client code
#		Name		- Client name
#		GLAccNumber - General ledger account
# Optional:
#		RegNumber - Company registration number / ID number of person
#		TaxReference - Company tax reference number
#		CreateSubAccount - Create sub GL account for this client
sub createClient {
	my (undef,$data) = @_;

	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Name'}) || $data->{'Name'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Name' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Name'} = $data->{'Name'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'RegNumber'} = $data->{'RegNumber'};
	$detail->{'TaxReference'} = $data->{'TaxReference'};
	$detail->{'CreateSubAccount'} = $data->{'CreateSubAccount'};
	my $res = wiaflos::server::core::Clients::createClient($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link address
# Parameters:
#		Code		- Client code
#		Type		- Address type, billing or shipping
#		Address		- Address
sub linkClientAddress {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Address'}) || $data->{'Address'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Address' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Address'} = $data->{'Address'};
	my $res = wiaflos::server::core::Clients::linkClientAddress($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link email address
# Parameters:
#		Code		- Client code
#		Type		- Address type, accounts or general
#		Address		- Email address
sub linkClientEmailAddress {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Address'}) || $data->{'Address'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Address' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Address'} = $data->{'Address'};
	my $res = wiaflos::server::core::Clients::linkClientEmailAddress($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link phone number
# Parameters:
#		Code		- Client code
#		Type		- Address type, billing or shipping
#		Number		- Phone number
# Optional:
#		Name		- Name of person/org
sub linkClientPhoneNumber {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Name'} = $data->{'Name'};
	my $res = wiaflos::server::core::Clients::linkClientPhoneNumber($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of client addresses
# Optional:
#		ID	 - Client ID
#		Code   - Client code
sub getClientAddresses {
	my (undef,$data) = @_;
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Check params
	if (!defined($data->{'ID'}) && !defined($data->{'Code'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' or 'Code' must be provided");
	}

	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'ID'} = $data->{'ID'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getClientAddresses($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Address'} = $item->{'Address'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of client email addresses
# Optional:
#		ID	 - Client ID
#		Code   - Client code
sub getClientEmailAddresses {
	my (undef,$data) = @_;
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Check params
	if (!defined($data->{'ID'}) && !defined($data->{'Code'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' or 'Code' must be provided");
	}

	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'ID'} = $data->{'ID'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getClientEmailAddresses($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Address'} = $item->{'Address'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of client phone numbers
# Optional:
#		ID	 - Client ID
#		Code   - Client code
sub getClientPhoneNumbers {
	my (undef,$data) = @_;
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Check params
	if (!defined($data->{'ID'}) && !defined($data->{'Code'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' or 'Code' must be provided");
	}

	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'ID'} = $data->{'ID'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getClientPhoneNumbers($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Number'} = $item->{'Number'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Remove client
# Parameters:
#		Code		- Client reference
sub removeClient {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Address' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	my $res = wiaflos::server::core::Clients::removeClient($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @fn getClientGLAccountEntries($data)
# Return list of supplier GL account entries
#
# @param data Parameter hash ref
# @li Code Supplier code
# @li StartDate Start date
# @li EndDate End date
#
# @returns Array ref of hash refs
# @li ID ID of GL account entry
# @li TransactionID Transaction ID
# @li Reference GL account entry reference
# @li TransactionDate Transaction date
# @li Amount Amount
# @li TransactionReference Transaction reference
sub getClientGLAccountEntries
{
	my (undef,$data) = @_;


	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'BalanceBroughtForward'} = $data->{'BalanceBroughtForward'};
	my $rawData = wiaflos::server::core::Clients::getGLAccountEntries($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'TransactionID'} = $item->{'TransactionID'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'TransactionReference'} = $item->{'TransactionReference'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# @fn createAccountTransaction($data)
# Create client GL account transaction
#
# @param data
# @li Code - Client Code
# @li Number - Transaction number, ie. TRN/xxyyzz
# @li Reference - GL account entry reference
# @li GLAccountNumber - GL account number to post this transaction to
# @li Date -Date of transaction
# @li Amount - Transaction amount
sub createAccountTransaction
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $tmp;
	$tmp->{'Code'} = $data->{'Code'};
	$tmp->{'Number'} = $data->{'Number'};
	$tmp->{'Reference'} = $data->{'Reference'};
	$tmp->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$tmp->{'Date'} = $data->{'Date'};
	$tmp->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::Clients::createAccountTransaction($tmp);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# @fn getAccountTransactions($data)
# Return an array of account transactions
#
# @param data Hash with following elements
# @li Code - Client code
# @li Type - Optional type, 'open' or 'all'
#
# @return Array ref of hash refs, described below
# @li ID - Transaction ID
# @li ClientID - Client ID
# @li Number - Transaction number
# @li Reference - Transaction reference
# @li GLAccountID - GL account ID
# @li TransactionDate - Transaction date
# @li Amount - Transaction amount
# @li GLTransactionID - GL transaction ID
# @li Posted - Posted, boolean
# @li Closed - Closed, boolean
sub getAccountTransactions
{
	my (undef,$data) = @_;


	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::Clients::getAccountTransactions($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'ClientID'} = $item->{'ClientID'};
		$tmpItem->{'Number'} = $item->{'Number'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'GLTransactionID'} = $item->{'GLTransactionID'};
		$tmpItem->{'Posted'} = $item->{'Posted'};
		$tmpItem->{'Closed'} = $item->{'Closed'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# @fn getAccountTransaction($data)
# Retrun an account transaction
#
# @param data Hash ref with the below attributes
# @li ID - Optional transaction ID
# @li Number - Optional transaction number
#
# @return Hash refs, described below
# @li ID - Transaction ID
# @li ClientID - Client ID
# @li Number - Transaction number
# @li Reference - Transaction reference
# @li GLAccountID - GL account ID
# @li TransactionDate - Transaction date
# @li Amount - Transaction amount
# @li GLTransactionID - GL transaction ID
# @li Posted - Posted, boolean
# @li Closed - Closed, boolean
sub getAccountTransaction
{
	my (undef,$data) = @_;
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Check params
	if (!defined($data->{'ID'}) && !defined($data->{'Number'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' or 'Number' must be provided");
	}

	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	$detail->{'Number'} = $data->{'Number'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Clients::getAccountTransaction($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};
	$tmpItem->{'ClientID'} = $rawData->{'ClientID'};
	$tmpItem->{'Number'} = $rawData->{'Number'};
	$tmpItem->{'Reference'} = $rawData->{'Reference'};
	$tmpItem->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$tmpItem->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$tmpItem->{'Amount'} = $rawData->{'Amount'};
	$tmpItem->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $rawData->{'Posted'};
	$tmpItem->{'Closed'} = $rawData->{'Closed'};

	return SOAPResponse(RES_OK,$tmpItem);
}


# @fn postAccountTransaction($data)
# Post an account transaction
#
# @param data Hash with the below elements...
# @li Number - Transaction number
#
# @return 0 on success, -1 on error
sub postAccountTransaction
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::Clients::postAccountTransaction($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Clients::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# @fn getAccountTransactionAllocations($data)
# Return an array of account transaction allocations
#
# @param data Hash with the below elements...
# @li Number - Account transaction number
#
# @return Array ref of hash refs
# @li ID - Account transaction ID
# @li Amount - Allocation amount
# @li CreditNoteID - Credit note ID to link to
# @li ReceiptAllocationID - Receipt allocation ID to link to
sub getAccountTransactionAllocations
{
	my (undef,$data) = @_;


	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $tmp;
	$tmp->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::Clients::getAccountTransactionAllocations($tmp);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Clients::Error());
	}

	# Build result
	my @data = ();
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'CreditNoteID'} = $item->{'CreditNoteID'};
		$tmpItem->{'ReceiptAllocationID'} = $item->{'ReceiptAllocationID'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


1;
# vim: ts=4
