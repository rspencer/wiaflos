# Tax functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Tax;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::GL;

# Use more precision here to calculate tax
use Math::BigFloat;
Math::BigFloat::precision(-4);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Get string representation of a tax mode
# Backend function, takes one parameter, the tax mode id, returns a string
sub getTaxModeStr
{
	my $mode = shift;


	# Decide what tax mode we have
	if ($mode eq "1") {
		return "incl";
	} elsif ($mode eq "2") {
		return "excl";
	}

	return "unknown";
}


# Get tax value for price, based on the current mode
# Backend function, takes 3 parameters, tax mode,  tax rate (percentage) and amount including tax.
# Returns: Math:BigFloat tax amount, tax mode ID
sub getTaxAmount
{
	my ($taxMode,$taxRate,$totalPrice) = @_;


	# Get ready for calculating tax
	my $taxAmount;
	# Check what mode we busy with
	if ($taxMode eq "incl" || $taxMode eq "1") {
		# Set mode
		$taxMode = 1;
		# Get tax amount
		$taxAmount = wiaflos::server::core::Tax::getTaxAmountFromIncl($taxRate,$totalPrice);
	} elsif ($taxMode eq "excl" || $taxMode eq "2") {
		# Set mode
		$taxMode = 2;
		# Get tax amount
		$taxAmount = wiaflos::server::core::Tax::getTaxAmountFromExcl($taxRate,$totalPrice);
	} else {
		setError("Invalid tax mode '$taxMode' provided");
		return undef;
	}

	return ($taxAmount,$taxMode);
}


# Get tax value for price including tax
# Backend function, takes 2 parameters, tax rate (percentage) and amount including tax, returns Math:BigFloat tax amount only
sub getTaxAmountFromIncl
{
	my ($taxRate_p,$amount_p) = @_;


	# Pull in params into math engine
	my $taxRate = Math::BigFloat->new($taxRate_p);
	my $amount = Math::BigFloat->new($amount_p);

	# Start off with zero
	my $taxAmount = Math::BigFloat->new(0);

	# If we actually have a rate, calculate
	if (!$taxRate->is_zero()) {
		# Get tax multiplier, 1.XX
		$taxRate->bdiv(100);
		$taxRate->badd(1);

		# Set amount including, divide by tax rate to get original amount without tax
		$taxAmount->badd($amount);
		$taxAmount->bdiv($taxRate);

		# Set amount to negative & add price, this will give us tax only
		$taxAmount->bmul(-1);
		$taxAmount->badd($amount);
	}

	# Reduce precision
	$taxAmount->precision(-2);

	return $taxAmount->bstr();
}


# Get tax value for price excluding tax
# Backend function, takes 2 parameters, tax rate (percentage) and amount including tax, returns Math:BigFloat tax amount only
sub getTaxAmountFromExcl
{
	my ($taxRate_p,$amount_p) = @_;


	# Pull in params into math engine
	my $taxRate = Math::BigFloat->new($taxRate_p);
	my $amount = Math::BigFloat->new($amount_p);

	# Start off with zero
	my $taxAmount = Math::BigFloat->new(0);

	# If we actually have a rate, calculate
	if (!$taxRate->is_zero()) {
		# Get tax multiplier, 0.XX
		$taxRate->bdiv(100);

		# Set amount excluding, multiply by tax rate to get amount
		$taxAmount->badd($amount);
		$taxAmount->bmul($taxRate);
	}

	# Reduce precision
	$taxAmount->precision(-2);

	return $taxAmount->bstr();
}


# Check if tax type exists
# Backend function, takes 1 parameter which is the invoice ID
sub taxTypeIDExists
{
	my $taxTypeID = shift;


	# Select tax type count
	my $rows = DBSelectNumResults("FROM tax_types WHERE ID = ".DBQuote($taxTypeID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return a tax type
# Parameters:
#		TaxTypeID	- Tax type ID
sub getTaxType
{
	my ($detail) = @_;


	# Return tax type details
	my $sth = DBSelect("
		SELECT
			ID, Description, GLAccountID, TaxRate
		FROM
			tax_types
		WHERE
			ID = ".DBQuote($detail->{'TaxTypeID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Description GLAccountID TaxRate ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding tax type");
		return ERR_NOTFOUND;
	}

	my $item;
	$item->{'ID'} = $row->{'ID'};
	$item->{'Description'} = $row->{'Description'};
	$item->{'GLAccountID'} = $row->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($row->{'GLAccountID'});
	$item->{'TaxRate'} = $row->{'TaxRate'};

	return $item;
}


# Create tax type
# Parameters:
#		Description		- Description
#		GLAccountNumber		- GL account
#		Rate		- Tax rate
sub createTaxType
{
	my ($detail) = @_;


	# Verify desc
	if (!defined($detail->{'Description'}) || $detail->{'Description'} eq "") {
		setError("No (or invalid) description provided");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No GL account provided");
		return ERR_PARAM;
	}

	# Verify rate
	if (!defined($detail->{'Rate'}) || $detail->{'Rate'} eq "") {
		setError("No tax rate provided");
		return ERR_PARAM;
	}

	# Pull in GL account
	my $GLAccID;
	# Check GL account exists
	if (($GLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccID;
	}

	# Add tax type
	my $sth = DBDo("
		INSERT INTO tax_types
				(Description,GLAccountID,TaxRate)
			VALUES
				(
					".DBQuote($detail->{'Description'}).",
					".DBQuote($GLAccID).",
					".DBQuote($detail->{'Rate'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("tax_types","ID");

	return $ID;
}





1;
# vim: ts=4
