# Supplier invoice payments
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Payments;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::Suppliers;
use wiaflos::server::core::GL;

use Math::BigFloat;



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Check if payment exists
# Backend function, takes 1 parameter which is the payment ID
sub supplierPaymentExists
{
	my $paymentID = shift;


	# Select payment count
	my $rows = DBSelectNumResults("FROM payments WHERE ID = ".DBQuote($paymentID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if payment number exists
sub supplierPaymentNumberExists
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^PMT/##;

	# Select payment count
	my $rows = DBSelectNumResults("FROM payments WHERE Number = ".DBQuote($number));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return payment id from number
sub getPaymentIDFromNumber
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^PMT/##;

	# Select payment
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			payments
		WHERE
			Number = ".DBQuote($number)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding payment '$number'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}




# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;
	$item->{'ID'} = $rawData->{'ID'};


	# Pull in supplier data
	$item->{'SupplierID'} = $rawData->{'SupplierID'};

	my $data;
	$data->{'ID'} = $rawData->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	$item->{'SupplierCode'} = $supplier->{'Code'};


	# Pull in GL account info
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});

	$item->{'Number'} = "PMT/".uc($rawData->{'Number'});

	$item->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$item->{'Reference'} = $rawData->{'Reference'};
	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Closed'} = $rawData->{'Closed'};

	return $item;
}


# Backend function to build item hash
sub sanitizeRawAllocationItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};


	$item->{'PaymentID'} = $rawData->{'PaymentID'};

   	# Pull in invoice data
	$item->{'SupplierInvoiceID'} = $rawData->{'SupplierInvoiceID'};


	my $data;
	$data->{'ID'} = $rawData->{'SupplierInvoiceID'};
	my $invoice = wiaflos::server::core::Purchasing::getSupplierInvoice($data);
	$item->{'SupplierInvoiceNumber'} = $invoice->{'Number'};


	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'Posted'} = defined($rawData->{'SupplierInvoiceTransactionID'}) ? 1 : 0;

	return $item;
}


# Create payment
# Parameters:
#		SupplierCode	- Supplier code
#		GLAccountNumber	- GL account where money was paid from
#		Number	- Reference for this payment
#		Date		- Date of payment
#		Reference			- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createPayment
{
	my ($detail) = @_;


	# Verify payment number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No (or invalid) payment number provided");
		return ERR_PARAM;
	}
	# Sanitize
	(my $paymentNumber = uc($detail->{'Number'})) =~ s#^PMT/##;

	# Verify supplier ref
	if (!defined($detail->{'SupplierCode'}) || $detail->{'SupplierCode'} eq "") {
		setError("No (or invalid) supplier code provided for payment '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No (or invalid) GL account provided for payment '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify date
	if (!defined($detail->{'Date'}) || $detail->{'Date'} eq "") {
		setError("No (or invalid) date provided for payment '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify reference
	if (!defined($detail->{'Reference'}) || $detail->{'Reference'} eq "") {
		setError("No (or invalid) reference provided for payment '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for payment '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Check if supplier exists
	my $supplierID;
	if (($supplierID = wiaflos::server::core::Suppliers::getSupplierIDFromCode($detail->{'SupplierCode'})) < 0) {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplierID;
	}

	# Check GL account exists
	my $GLAccountID;
	if (($GLAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccountID;
	}

	# Check for conflicts
	if (supplierPaymentNumberExists($paymentNumber)) {
		setError("Payment number '$paymentNumber' already exists");
		return ERR_CONFLICT;
	}

	# Create payment
	my $sth = DBDo("
		INSERT INTO payments
				(SupplierID,GLAccountID,Number,TransactionDate,Reference,Amount,Closed)
			VALUES
				(
					".DBQuote($supplierID).",
					".DBQuote($GLAccountID).",
					".DBQuote($paymentNumber).",
					".DBQuote($detail->{'Date'}).",
					".DBQuote($detail->{'Reference'}).",
					".DBQuote($detail->{'Amount'}).",
					0
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("payments","ID");

	return $ID;
}


# Return an array of payments
# Optional
#		Type - "open", "all"
sub getPayments
{
	my ($detail) = @_;
	my $type = defined($detail->{'Type'}) ? $detail->{'Type'} : "open";
	my @payments = ();


	# Return list of payments
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, GLAccountID, Number, TransactionDate,
			Reference, Amount, Closed
		FROM
			payments
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID GLAccountID Number TransactionDate
				Reference Amount Closed )
	)) {
		# Check what kind of payments we want
		if (($type eq "open") && $row->{'Closed'} eq "0") {
			push(@payments,sanitizeRawItem($row));
		} elsif ($type eq "all") {
			push(@payments,sanitizeRawItem($row));
		}
	}

	DBFreeRes($sth);

	return \@payments;
}


# Return an payment hash
# Optional:
#		Number	- Payment number
#		ID		- Payment ID
sub getPayment
{
	my ($detail) = @_;


	my $paymentID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify payment number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) payment number provided");
			return ERR_PARAM;
		}

		# Check if payment exists
		if (($paymentID = getPaymentIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $paymentID;
		}
	} else {
		$paymentID = $detail->{'ID'};
	}

	# Verify payment ID
	if (!$paymentID || $paymentID < 1) {
		setError("No (or invalid) payment number/id provided");
		return ERR_PARAM;
	}

	# Return list of payments
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, GLAccountID, Number, TransactionDate, Reference, Amount, GLTransactionID, Closed
		FROM
			payments
		WHERE
			payments.ID = ".DBQuote($paymentID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID GLAccountID Number TransactionDate Reference Amount GLTransactionID Closed )
	);
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}


# One can now post the payment, which sets GLTransactionID
# Parameters:
#		Number	- Payment number
sub postPayment
{
	my ($detail) = @_;


	# Verify payment number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No (or invalid) payment number provided");
		return ERR_PARAM;
	}

	my $data;

	# Check if payment exists & pull in
	$data = undef;
	$data->{'Number'} = $detail->{'Number'};
	my $payment = getPayment($data);
	if (ref $payment ne "HASH") {
		setError(Error());
		return $payment;
	}

	# Make sure payment is not posted
	if ($payment->{'Posted'} eq "1") {
		setError("Payment '".$payment->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Pull in supplier
	$data = undef;
	$data->{'ID'} = $payment->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $payment;
	}

	DBBegin();

	# Create transaction
	my $transactionRef = sprintf('Payment: %s',$payment->{'Number'});
	$data = undef;
	$data->{'Date'} = $payment->{'TransactionDate'};
	$data->{'Reference'} = $transactionRef;
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# Pull in amount
	my $transValue = Math::BigFloat->new($payment->{'Amount'});
	$transValue->precision(-2);

	# Link to supplier GL account
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'GLAccountID'} = $supplier->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Negate for other side
	$transValue->bmul(-1);

	# Link from GL
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'Reference'} = $payment->{'Reference'};
	$data->{'GLAccountID'} = $payment->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post payment
	my $sth = DBDo("
		UPDATE payments
		SET
			GLTransactionID = ".DBQuote($GLTransactionID)."
		WHERE
			ID = ".DBQuote($payment->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return $GLTransactionID;
}


# Create allocation
# Parameters:
#		PaymentNumber	- Payment number
#		SupplierInvoiceNumber	- Supplier invoice number
#		Amount		- Amount
sub createPaymentAllocation
{
	my ($detail) = @_;


	# Verify payment number
	if (!defined($detail->{'PaymentNumber'}) || $detail->{'PaymentNumber'} eq "") {
		setError("No (or invalid) payment number provided");
		return ERR_PARAM;
	}

	# Verify invoice number
	if (!defined($detail->{'SupplierInvoiceNumber'}) || $detail->{'SupplierInvoiceNumber'} eq "") {
		setError("No (or invalid) supplier invoice number provided for payment '".$detail->{'PaymentNumber'}."'");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for payment '".$detail->{'PaymentNumber'}."'");
		return ERR_PARAM;
	}
	my $allocAmount = Math::BigFloat->new($detail->{'Amount'});
	$allocAmount->precision(-2);
	if ($allocAmount->is_zero()) {
		setError("Allocation amount cannot be zero on payment '".$detail->{'PaymentNumber'}."'");
		return ERR_AMTZERO;
	}

	my $data;

	# Check if payment exists & pull in
	$data = undef;
	$data->{'Number'} = $detail->{'PaymentNumber'};
	my $payment = getPayment($data);
	if (ref $payment ne "HASH" ) {
		setError(Error());
		return $payment;
	}

	# Make sure payment is posted
	if ($payment->{'Posted'} ne "1") {
		setError("Requested payment not posted");
		return ERR_POSTED;
	}

	# Grab invoice
	$data = undef;
	$data->{'Number'} = $detail->{'SupplierInvoiceNumber'};
	my $invoice = wiaflos::server::core::Purchasing::getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(wiaflos::server::core::Purchasing::Error());
		return $invoice;
	}

	# Invoice must be posted before we can allocate a payment
	if ($invoice->{'Posted'} ne "1") {
		setError("Cannot allocate a payment to supplier invoice '".$invoice->{'Number'}."' as its not posted");
		return ERR_POSTED;
	}

	# Grab allocations
	$data = undef;
	$data->{'PaymentID'} = $payment->{'ID'};
	my $allocs = getPaymentAllocations($data);
	if (ref $allocs ne "ARRAY") {
		return $allocs;
	}

	# Check if we either balance to 0 or have left over
	my $paymentBalance = Math::BigFloat->new($payment->{'Amount'});
	$paymentBalance->precision(-2);
	foreach my $alloc (@{$allocs}) {
		$paymentBalance->bsub($alloc->{'Amount'});
	}
	$paymentBalance->bsub($detail->{'Amount'});
	if ($paymentBalance->is_neg()) {
		setError("Creating this allocation will exceed payment '".$payment->{'PaymentNumber'}."' amount by ".$paymentBalance->bstr());
		return ERR_OVERALLOC;
	}

	# Create payment allocation
	my $sth = DBDo("
		INSERT INTO payment_allocations
				(PaymentID,SupplierInvoiceID,Amount)
			VALUES
				(
					".DBQuote($payment->{'ID'}).",
					".DBQuote($invoice->{'ID'}).",
					".DBQuote($detail->{'Amount'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("payment_allocations","ID");


	return $ID;
}


# Return an array of payment allocations
# Parameters:
#		PaymentNumber	- Payment number
#		PaymentID	- Payment ID
#		SupplierInvoiceID	- Supplier invoice ID
sub getPaymentAllocations
{
	my ($detail) = @_;

	my @allocations = ();


	# SQL query string to use
	my $query = "";

	# ID mode
	if (defined($detail->{'PaymentID'}) && $detail->{'PaymentID'} > 0) {
		$query .= "PaymentID = ".$detail->{'PaymentID'};

	# Payment ref mode
	} elsif (defined($detail->{'PaymentNumber'}) && $detail->{'PaymentNumber'} ne "") {
		my $paymentID;

		# Check if payment exists
		if (($paymentID = getPaymentIDFromNumber($detail->{'PaymentNumber'})) < 1) {
			setError(Error());
			return $paymentID;
		}

		$query .= "PaymentID = ".DBQuote($paymentID);

	# Invoice ID mode
	} elsif (defined($detail->{'SupplierInvoiceID'}) && $detail->{'SupplierInvoiceID'} ne "") {
		$query .= "SupplierInvoiceID = ".DBQuote($detail->{'SupplierInvoiceID'});

	} else {
		setError("No acceptable parameters provided when getting payment allocations");
		return ERR_USAGE;
	}

	# Return list of payment allocations
	my $sth = DBSelect("
		SELECT
			ID, SupplierInvoiceID, Amount, SupplierInvoiceTransactionID
		FROM
			payment_allocations
		WHERE
			$query
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierInvoiceID Amount SupplierInvoiceTransactionID )
	)) {
		push(@allocations,sanitizeRawAllocationItem($row));
	}

	DBFreeRes($sth);

	return \@allocations;
}


# Return an hash containing a allocation
# Parameters:
#		ID	- Allocation ID
sub getPaymentAllocation
{
	my ($detail) = @_;

	# Verify allocation id
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	# Return allocation
	my $sth = DBSelect("
		SELECT
			ID, PaymentID, SupplierInvoiceID, Amount, SupplierInvoiceTransactionID
		FROM
			payment_allocations
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID PaymentID SupplierInvoiceID Amount SupplierInvoiceTransactionID ));
	DBFreeRes($sth);

	return sanitizeRawAllocationItem($row);
}


# One can now post the allocation, which checks the invoice if its totally paid
# Parameters:
#		ID	- Payment allocation ID
sub postPaymentAllocation
{
	my ($detail) = @_;


	# Verify allocation ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	my $data;

	# Check if allocation exists & pull in
	$data = undef;
	$data->{'ID'} = $detail->{'ID'};
	my $allocation = getPaymentAllocation($data);
	if (ref $allocation ne "HASH") {
		setError(Error());
		return $allocation;
	}

	# Get corrosponding payment
	$data = undef;
	$data->{'ID'} = $allocation->{'PaymentID'};
	my $payment = getPayment($data);
	if (ref $payment ne "HASH") {
		setError(Error());
		return $payment;
	}

	# Make sure allocation is not posted
	if ($allocation->{'Posted'} ne "0") {
		setError("Allocation '".$allocation->{'ID'}."' on payment '".$payment->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Grab invoice
	$data = undef;
	$data->{'ID'} = $allocation->{'SupplierInvoiceID'};
	my $invoice = wiaflos::server::core::Purchasing::getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(wiaflos::server::core::Purchasing::Error());
		return $invoice;
	}

	# Make sure invoice is not paid
	if ($invoice->{'Paid'} ne "0") {
		setError("Supplier invoice '".$invoice->{'Number'}."' already paid");
		return ERR_PAID;
	}

	# Make sure invoice is posted
	if ($invoice->{'Posted'} ne "1") {
		setError("Supplier invoice '".$invoice->{'Number'}."' not posted");
		return ERR_POSTED;
	}

	# Grab allocations
	$data = undef;
	$data->{'SupplierInvoiceID'} = $allocation->{'SupplierInvoiceID'};
	my $allocs = getPaymentAllocations($data);
	if (ref $allocs ne "ARRAY") {
		setError(Error());
		return $allocs;
	}

	# Check if we either balance to 0 or have left over
	my $invBalance = Math::BigFloat->new($invoice->{'Total'});
	$invBalance->precision(-2);
	foreach my $item (@{$allocs}) {
		if ($item->{'Posted'} eq "1") {
			$invBalance->bsub($item->{'Amount'});
		}
	}
	# Check invoice balance if its negative, this is if we've overallocated
	$invBalance->bsub($allocation->{'Amount'});
	if ($invBalance->is_neg()) {
		setError("Posting the allocation will end up in a balance of '".$invBalance->bstr()."' on invoice '".$invoice->{'Number'}."'");
		return ERR_NOBALANCE;
	}

	# Grab payment allocations
	$data = undef;
	$data->{'PaymentID'} = $payment->{'ID'};
	my $paymentAllocations = getPaymentAllocations($data);
	if (ref $paymentAllocations ne "ARRAY") {
		setError(Error());
		return $paymentAllocations;
	}
	# Add up payment balance
	my $paymentBalance = Math::BigFloat->new($payment->{'Amount'});
	foreach my $alloc (@{$paymentAllocations}) {
		if ($alloc->{'Posted'} eq "1") {
			$paymentBalance->bsub($alloc->{'Amount'});
		}
	}
	# Check payment balance if its negative, this is if we've overallocated
	$paymentBalance->bsub($allocation->{'Amount'});
	if ($paymentBalance->is_neg()) {
		setError("Posting the allocation will end up in a balance of '".$paymentBalance->bstr()."' on payment '".$payment->{'Number'}."'");
		return ERR_OVERALLOC;
	}

	DBBegin();

	# If we equal out our payment, mark it as being closed
	if ($paymentBalance->is_zero()) {
		# Close it off
		my $sth = DBDo("
			UPDATE
				payments
			SET
				Closed = 1
			WHERE
				ID = ".DBQuote($payment->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}

	# Create supplier invoice transaction
	$data = undef;
	$data->{'ID'} = $invoice->{'ID'};
	$data->{'Amount'} = Math::BigFloat->new($allocation->{'Amount'})->bneg();
	$data->{'PaymentAllocationID'} = $allocation->{'ID'};
	my $supplierInvoiceTransactionID = wiaflos::server::core::Purchasing::allocateSupplierInvoiceTransaction($data);
	if ($supplierInvoiceTransactionID < 1) {
		setError(wiaflos::server::core::Purchasing::Error());
		DBRollback();
		return $supplierInvoiceTransactionID;
	}

	# Post allocation
	my $sth = DBDo("
		UPDATE payment_allocations
		SET
			SupplierInvoiceTransactionID = ".DBQuote($supplierInvoiceTransactionID)."
		WHERE
			ID = ".DBQuote($allocation->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
