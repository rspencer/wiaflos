# Templating functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::templating;

use strict;
use warnings;


use Template;
use Template::Exception;


# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	loadTemplate
);
@EXPORT_OK = qw(
);


# Server instance
my $config;


# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Initialize templating engine
sub Init
{
	my $server = shift;

	$config = $server->{'template_config'};
};


# Create a template object
# Args: template_name params
sub loadTemplate
{
	my ($templateName,$params,$output) = @_;


	# Build config
	my $cfg = {
		# Interpolate variables in-place
		INTERPOLATE => 1,
		# Include paths, separated by :
		INCLUDE_PATH => $config->{'path'},
		# Header
		PRE_PROCESS => $config->{'global_header'},
		# Footer
		POST_PROCESS => $config->{'global_footer'},
		# Debug
		DEBUG => $config->{'debug'},
	};

	# Create template object
	my $template = Template->new($cfg);
	if (!$template) {
		setError($Template::ERROR);
		return undef;
	}

	# Process
	my $res = $template->process($templateName,$params,$output);
	if (!$res) {
		setError($template->error());
		return undef;
	}

	return $output;
}


# Internal function to generate a template error
# Args: template_name params
sub abort
{
	die Template::Exception->new("abort",join(': ',@_));
}



1;
# vim: ts=4
