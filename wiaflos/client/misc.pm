# Misc functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::misc;

use strict;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
	parseArgs
);



# Parse the commandline into parameters
sub parseArgs
{
	my (@args) = @_;
	my $res;

	foreach my $arg (@args) {
		my ($attr,$value) = split /=/,$arg;

		if (defined($value)) {
			$res->{$attr} = $value;
			$res->{$attr} =~ s/^['"]//;
			$res->{$attr} =~ s/['"]$//;
		} else {
			$res->{$attr} = 1;
		}
	}

	return $res;
}



1;
# vim: ts=4
