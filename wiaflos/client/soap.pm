# Soap functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::soap;

use strict;
use warnings;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
	soapCall
	soapError
	soapFault
	soapConnected
	soapConnect
	soapDebug
);

use wiaflos::constants;
use wiaflos::soap;

use SOAP::Lite;




# SOAP error container
my $soapError;

# SOAP server details
my $soapURI;


# Call soap function
sub soapCall
{
	my ($OUT,$module,$function,@params) = @_;


	# Check if we've "connected"
	if (!defined($soapURI)) {
		return SOAPResponse(RES_ERROR,-1,"Login first");
	}

	# Check if this is a local call or not
	if ($soapURI eq "local") {


		# Make sure the class has ::'s instead of /'s
		my $class = $module;
		$class =~ s,/,::,g;

		# Call function directly
		my $data;
		my $res = eval("
			${class}::${function}(undef,\@params);
		");
		if ($@) {
			# If $res is not defined, blank it
			if (!defined($res)) {
				$res = "";
			}

			return SOAPResponse(RES_ERROR,-1,"Error: $res $@");
		}


		return $res;

	} else {
		# Setup SOAP using session key
		my $soap = SOAP::Lite
			-> uri($module)
			-> proxy($soapURI)
			-> on_fault( \&soapFault );

		# Call soap function
		my $rslt = $soap->call("$function" => @params);

		# Check transport error
		if (my $res = soapError()) {
			return SOAPResponse(RES_ERROR,-1,"Transport error: $res");
		}

		return $rslt->result;
	}

}


# Setup soap error container
sub soapFault
{
	my($soap, $res) = @_;
	$soapError = ref $res ? $res->faultstring : $soap->transport->status;
}


# Return soap error, if we have one, or just default to undef
sub soapError
{
	my $error = $soapError;
	$soapError = undef;
	return $error;
}

# Return if we connected to SOAP
#sub soapConnected
#{
#	return defined($soapSession) ? 1 : 0;
#}

# Connect to SOAP server
sub soapConnect
{
	my ($OUT,@args) = @_;


	# Special case
	if (@args == 1 && $args[0] eq "local") {
		$soapURI = "local";
		return 0;
	}


	# Validate params
	if (@args < 2) {
		return -1;
	}

	# Setup details we need
	my $username = $args[0];
	my $password = $args[1];
	$soapURI = $args[2] ? $args[2] : "http://$username:$password\@localhost:1080";

#	print($OUT " => Connecting to $soapURI...");

#	# Setup SOAP
#	my $soap = SOAP::Lite
#		-> uri('Session')
#		-> proxy("$soapURI/Auth:User")
#		-> on_fault( \&soapFault );

#	my $rslt = $soap->getSessionKey();

#	# Check transport error
#	if (my $res = soapError()) {
#		print($OUT "ERROR: $res\n");
#		return -1;
#	}

#	print($OUT "\n");

#	$soapSession = $rslt->result();

	return 0;
}


# SOAP debug stuff
sub soapDebug
{
		my ($OUT,$res) = @_;
		print($OUT "    => SOAP ERROR: ".$res->{'Data'}."\n");
		print($OUT "    => DEBUG: ".$res->{'Info'}."\n") if (defined($res->{'Info'}));
}


1;
# vim: ts=4
