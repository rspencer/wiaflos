# Gtk user interface
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::gtk;

use strict;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
	execute
);


use wiaflos::version;
use wiaflos::client::config;
use wiaflos::client::soap;
use wiaflos::client::misc;

use SOAP::Lite;


sub menuitem_cb {
  my ($callback_data, $callback_action, $widget) = @_;

  my $dialog = Gtk2::MessageDialog->new($callback_data,
										 'destroy-with-parent',
										 'info',
										 'close',
										 sprintf "You selected or toggled the menu item: \"%s\"",
									Gtk2::ItemFactory->path_from_widget ($widget));

  # Close dialog on user response
  $dialog->signal_connect(response => sub { $dialog->destroy; 1 });

  $dialog->show();
}


sub connect {
}

sub menuitem_file_connect_cb {
	my ($callback_data, $callback_action, $widget) = @_;


	my $dialog = Gtk2::Dialog->new("Connect...", $callback_data, 'destroy-with-parent',
			'gtk-cancel' => 'cancel',
			'Do it'	  => 'ok');

	$dialog->show();
}



sub menuitem_file_quit_cb {
	my ($callback_data, $callback_action, $widget) = @_;

	Gtk2->main_quit();
}



# Help/About Callback
sub menuitem_help_about_cb {
	my ($callback_data, $callback_action, $widget) = @_;
	my $text;


	# Create and set app name
	my $dialog = Gtk2::AboutDialog->new;
	$dialog->set_name("$APPNAME v$VERSION");

	# Pull in authors
	$text = "";
	open(FILE,"< AUTHORS");
	while (my $line = <FILE>) {
		$text .= $line;
	}
	$dialog->set_authors($text);
	close(FILE);

	$dialog->set_copyright($COPYRIGHT);
	$dialog->set_website($APPURL);

	# Pull in license
	$text = "";
	open(FILE,"< COPYING");
	while (my $line = <FILE>) {
		$text .= $line;
	}
	$dialog->set_license($text);
	close(FILE);

	# Close dialog on user response
	$dialog->signal_connect (response => sub { $dialog->destroy; 1 });

	$dialog->show;
}




# Update statusbar function
sub update_statusbar {
  my ($buffer, $statusbar) = @_;

  $statusbar->pop(0); # clear any previous message, underflow is allowed

  $statusbar->push(0,"Hello world");
}






# Start everything up
sub ui_start
{
	use Glib ':constants';
	use Gtk2;


my @entries = (
  # name,		stock id,	label
  [ "FileMenu",		undef,		"_File"],
  [ "ClientsMenu",	undef,		"_Clients"],
  [ "HelpMenu",		undef,		"_Help"],
  # name,	  stock id,  label,	accelerator,  tooltip
  [ "List",		undef,		"List",  undef, "Client List",		\&menuitem_cb  ],

  [ "Connect",		undef,		"Connect",  undef, "Connect",		\&menuitem_file_connect_cb  ],
  [ "Quit",		'gtk-quit',	"Quit",  undef, "Quit",			  \&menuitem_file_quit_cb  ],

  [ "About",  undef,	  "_About", "<control>A", "About",			 \&menuitem_help_about_cb ],
);


	# Create menu items
	my $menu_items = "
<ui>
  <menubar name='MenuBar'>
	<menu action='FileMenu'>
	  <menuitem action='Connect'/>
	  <menuitem action='Quit'/>
	</menu>
	<menu action='ClientsMenu'>
	  <menuitem action='List'/>
	</menu>
	<menu action='HelpMenu'>
	  <menuitem action='About'/>
	</menu>
  </menubar>
</ui>
	";






	# Init Gtk
	Gtk2::Gdk::Threads->init();
	Gtk2->init();
	die "Glib::Object thread safetly failed"
		unless Glib::Object->set_threadsafe (TRUE);


	# Setup main window
	my $window = Gtk2::Window->new('toplevel');
	$window->set_title($APPSTRING);



	# Setup signals
	$window->signal_connect(
			'destroy' => sub {
					Gtk2->main_quit();
					1;
			}
	);

	# Set size of window
	$window->set_size_request(800,600);


	# Create vertical box - vertically setup window
	my $vbox = Gtk2::VBox->new(FALSE,0);
	$window->add($vbox);



	# Create menu
	my $actions = Gtk2::ActionGroup->new ("Actions");
	$actions->add_actions (\@entries, undef);
	# Action group binds menu items
	my $ui = Gtk2::UIManager->new;
	$ui->insert_action_group($actions, 0);
	# Accel group is for key shortcuts
	$window->add_accel_group ($ui->get_accel_group);
	# Build menu items and the UI
	$ui->add_ui_from_string($menu_items);

	# Attach menu
	$vbox->pack_start($ui->get_widget("/MenuBar"),FALSE,FALSE,0);



	# Create scrolled window
	my $viewport = Gtk2::ScrolledWindow->new;
	$viewport->set_shadow_type('in');
	$viewport->set_policy('automatic','automatic');
	$vbox->pack_start($viewport,TRUE,TRUE,0);



	# Create statusbar
	my $statusbar = Gtk2::Statusbar->new;
	$vbox->pack_start($statusbar,FALSE,FALSE,0);

	update_statusbar ("hello", $statusbar);




	$window->show_all();

	Gtk2->main();
}



sub execute
{
	ui_start;
}


1;
# vim: ts=4
