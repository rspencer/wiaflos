# Statement functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Statements;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Statements",
	Menu 	=> [
		# Clients Menu
		{
			MenuItem 	=> "Clients",
			Children 	=> [
				{
					MenuItem 	=> "Statements",
					Regex		=> "state(?:ments)?",
					Children	=> [
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Display statement",
							Help		=> 'show client="<client code>" [start="<start date>"]',
							Function	=> \&show,
						},
						{
							MenuItem 	=> "Send",
							Regex		=> "send",
							Desc		=> "Send statement",
							Help		=> 'send client="<client code>" start="<yyyy-mm-dd>" [subject="<subject>"] sendto="<email[:addy1,addy2...] or file:filename>"',
							Function	=> \&send,
						},
					],
				},
			],
		},
	],
};



# Show client statement
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'ClientCode'} = $parms->{'client'};
	$detail->{'StartDate'} = $parms->{'start'};
	my $res = soapCall($OUT,"wiaflos/server/api/Statements","getStatement",$detail);

	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "Date", "Ref", "Amount", "Balance");
+===========+============+================================================================================+==============+================+
| @|||||||| | @||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||| | @||||||||||||| |
+===========+============+================================================================================+==============+================+
END
		# Sort data
		my $balance = Math::BigFloat->new(0);

		foreach my $entry (@{$res->{'Data'}}) {
			$balance->badd($entry->{'Amount'});
			print swrite(<<'END', $entry->{'ID'}, $entry->{'TransactionDate'}, $entry->{'Reference'} ? $entry->{'Reference'} : $entry->{'TransactionReference'}, sprintf('%.2f',$entry->{'Amount'}),sprintf('%.2f',$balance));
| @<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}
		print swrite(<<'END');
+===========+============+================================================================================+==============+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Send statement
sub send
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'sendto'})) {
		print($OUT "  => ERROR: Parameter 'sendto' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'ClientCode'} = $parms->{'client'};
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'SendTo'} = $parms->{'sendto'};
	$detail->{'Subject'} = $parms->{'subject'};
	my $res = soapCall($OUT,"wiaflos/server/api/Statements","sendStatement",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}




1;
# vim: ts=4
