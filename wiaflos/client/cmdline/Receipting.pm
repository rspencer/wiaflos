# Receipting functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Receipting;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Receipting",
	Menu 	=> [
		# Clients main menu option
		{
			MenuItem 	=> "Clients",
			Children 	=> [
				# Receipts
				{
					MenuItem 	=> "Receipts",
					Regex		=> "receipt(?:s|ing)",
					Children => [
						{
							MenuItem 	=> "CreateReceipt",
							Regex		=> "create",
							Desc		=> "Create a receipt",
							Help		=> 'create client="<client code>" account="<source of receipt>" number="<receipt number>" date="<receipt date>" reference="<journal ref>" amount="<amount paid>"',
							Function	=> \&createReceipt,
						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "List receipts",
							Help		=> 'list [type="<all|open>"]',
							Function	=> \&list,
						},
						{
							MenuItem 	=> "Send",
							Regex		=> "send",
							Desc		=> "Send receipt",
							Help		=> 'send receipt="<receipt number>" sendto="<email[:addy1,addy2...] or file:filename>"',
							Function	=> \&send,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "Post receipt",
							Help		=> 'post receipt="<receipt number>"',
							Function	=> \&postReceipt,
						},
						{
							MenuItem 	=> "Allocate",
							Regex		=> "alloc(?:ate)?",
							Desc		=> "Allocate monies from a receipt",
							Help		=> 'allocate receipt="<receipt number>" [invoice="<client invoice>"] [transaction="<transaction number>"] amount="<amount to allocate>"',
							Function	=> \&createAllocation,
						},
						{
							MenuItem 	=> "PostAllocation",
							Regex		=> "postalloc(?:ation)?",
							Desc		=> "Post receipt allocation",
							Help		=> 'postAllocation id="<allocation ID>>"',
							Function	=> \&postAllocation,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Show receipt allocations",
							Help		=> 'show receipt="<receipt number>"',
							Function	=> \&show,
						},
					],
				},
			],
		},
	],
};



# Last ID info
my %last;


# Create receipt
sub createReceipt
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'date'})) {
		print($OUT "  => ERROR: Parameter 'date' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'reference'})) {
		print($OUT "  => ERROR: Parameter 'reference' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'ClientCode'} = $parms->{'client'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'Date'} = $parms->{'date'};
	$detail->{'Reference'} = $parms->{'reference'};
	$detail->{'Amount'} = $parms->{'amount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","createReceipt",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# List receipts
sub list
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	# build request
	my $detail;
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","getReceipts",$detail);

	if ($res->{'Result'} == RES_OK) {
			print swrite(<<'END', "ID", "Client", "GLAccRef", "Receipt", "TransDate", "Ref", "Amount");
Receipt list
+========+============+==============+============================+============+=================================================+============+
| @||||| | @||||||||| | @||||||||||| | @||||||||||||||||||||||||| | @||||||||| | @|||||||||||||||||||||||||||||||||||||||||||||| | @||||||||| |
+========+============+==============+============================+============+=================================================+============+
END

		foreach my $receipt (@{$res->{'Data'}}) {
				print swrite(<<'END', $receipt->{'ID'}, $receipt->{'ClientCode'}, $receipt->{'GLAccountNumber'}, $receipt->{'Number'}, $receipt->{'TransactionDate'}, $receipt->{'Reference'}, sprintf('%.2f',$receipt->{'Amount'}));
| @<<<<< | @<<<<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>> |
END
		}
			print swrite(<<'END');
+========+============+==============+============================+============+=================================================+============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Post receipt
sub postReceipt
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'receipt'})) {
		print($OUT "  => ERROR: Parameter 'receipt' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'receipt'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","postReceipt",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create allocation
sub createAllocation
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'receipt'})) {
		print($OUT "  => ERROR: Parameter 'receipt' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'invoice'}) && !defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'invoice' and 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'ReceiptNumber'} = $parms->{'receipt'};
	$detail->{'InvoiceNumber'} = $parms->{'invoice'};
	$detail->{'TransactionNumber'} = $parms->{'transaction'};
	$detail->{'Amount'} = $parms->{'amount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","createReceiptAllocation",$detail);
	# Save this ID
	if ($res->{'Result'} == RES_OK) {
		# Save this ID
		$last{'allocationid'} = $res->{'Data'};
	 } else {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Post allocation
sub postAllocation
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'id'})) {
		print($OUT "  => ERROR: Parameter 'id' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'id'} = $last{'allocationid'} if ($parms->{'id'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'id'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","postReceiptAllocation",$detail);
	if ($res->{'Result'} == RES_OK) {
		# Null the id
		$last{'allocationid'} = undef;
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show receipt allocations
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'receipt'})) {
		print($OUT "  => ERROR: Parameter 'receipt' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	my $res;

	$detail->{'Number'} = $parms->{'receipt'};
	$res = soapCall($OUT,"wiaflos/server/api/Receipting","getReceipt",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $receipt = $res->{'Data'};

	$detail = undef;
	$detail->{'ReceiptNumber'} = $parms->{'receipt'};
	$res = soapCall($OUT,"wiaflos/server/api/Receipting","getReceiptAllocations",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $allocs = $res->{'Data'};


	print swrite(<<'END', $detail->{'ReceiptNumber'}, "ID", "Invoice", "Amount", "Posted", "Balance");
Receipt allocations for: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
+=======+=====================+==============+========+==============+
| @|||| | @|||||||||||||||||| | @||||||||||| | @||||| | @||||||||||| |
+=======+=====================+==============+========+==============+
END
	my $balance = Math::BigFloat->new(0); $balance->precision(-2);
	my $unposted = Math::BigFloat->new(0); $unposted->precision(-2);

	foreach my $alloc (@{$allocs}) {
		$balance->badd($alloc->{'Amount'}) if ($alloc->{'Posted'});
		$unposted->badd($alloc->{'Amount'}) if (!$alloc->{'Posted'});
		print swrite(<<'END', $alloc->{'ID'}, defined($alloc->{'InvoiceNumber'}) ? $alloc->{'InvoiceNumber'} : 'Transaction', sprintf('%.2f',$alloc->{'Amount'}), $alloc->{'Posted'}, $alloc->{'Posted'} ? $balance->bstr() : '');
| @<<<< | @<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @||||| | @>>>>>>>>>>> |
END
	}

	my $available = Math::BigFloat->new($receipt->{'Amount'});
	$available->bsub($balance);

	$balance->bmul(-1);

	print swrite(<<'END',sprintf('%.2f',$receipt->{'Amount'}),sprintf('%.2f',$balance->bstr()),sprintf('%.2f',$unposted->bstr()),sprintf('%.2f',$available->bstr()));
+=======+=====================+==============+========+==============+
|                                      Receipt Amount | @>>>>>>>>>>> |
|                                     Allocated Funds | @>>>>>>>>>>> |
|                                      Unposted Funds | @>>>>>>>>>>> |
+=====================================================+==============+
|                                     Available Funds | @>>>>>>>>>>> |
+=====================================================+==============+
END

	return RES_OK;
}


# Send receipt
sub send
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'receipt'})) {
		print($OUT "  => ERROR: Parameter 'receipt' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'sendto'})) {
		print($OUT "  => ERROR: Parameter 'sendto' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'receipt'};
	$detail->{'SendTo'} = $parms->{'sendto'};
	my $res = soapCall($OUT,"wiaflos/server/api/Receipting","sendReceipt",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}




1;
# vim: ts=4
